package com.khrystyna.model;

import java.util.Random;

public final class CountryGenerator {
    private Random random = new Random();
    private final String[][] countries = {{"AFGHANISTAN", "KABUL"},
            {"ALBANIA", "TIRANA"},
            {"ANGOLA", "LUANDA"},
            {"BELGIUM", "BRUSSELS"},
            {"BELIZE", "BELMOPAN"},
            {"CANADA", "OTTAWA"},
            {"CHINA", "BEIJING"},
            {"MEXICO", "MEXICO CITY"},
            {"POLAND", "WARSAW"},
            {"SPAIN", "MADRID"},
            {"SWEDEN", "STOCKHOLM"},
            {"THAILAND", "BANGKOK"},
            {"TURKEY", "ANKARA"},
            {"UKRAINE", "KIEV"},
            {"UNITED KINGDOM", "LONDON"},
    };

    public Country getNextCountry() {
        int index = random.nextInt(countries.length);
        return new Country(countries[index][0], countries[index][1]);
    }
}
