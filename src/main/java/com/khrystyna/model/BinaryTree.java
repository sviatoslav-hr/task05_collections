package com.khrystyna.model;

import java.util.*;

/**
 * Simple binary tree that maps elements using pair key-value.
 *
 * @param <K> key
 * @param <V> value
 * @author Sviatoslav Khrystyna
 * @since 29.04.1999
 */
public class BinaryTree<K extends Comparable<K>, V> implements Map<K, V> {
    /**
     * A tree root entry.
     */
    private Entry<K, V> root;
    /**
     * Number of stored elements.
     */
    private int size;

    /**
     * A tree entry (key-value pair).
     *
     * @param <E> key
     * @param <F> value
     */
    class Entry<E, F> implements Map.Entry<E, F>, TreePrinter.PrintableNode {
        private Entry<E, F> leftChild;
        private Entry<E, F> rightChild;
        private E key;
        private F value;

        Entry(E key, F value) {
            this.key = key;
            this.value = value;
        }

        public Entry<E, F> getLeftChild() {
            return leftChild;
        }

        public Entry<E, F> getRightChild() {
            return rightChild;
        }

        @Override
        public String getText() {
            return key + ":" + value;
        }

        public E getKey() {
            return key;
        }

        public F getValue() {
            return value;
        }

        public F setValue(F value) {
            this.value = value;
            return this.value;
        }
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean containsKey(Object key) {
        return getByKeyFrom(key, root) != null;
    }

    @Override
    public boolean containsValue(Object value) {
        return getByValueFrom(value, root) != null;
    }

    /**
     * If finds equal value, returns the node inside with the specified value is mapped,
     * otherwise if current node has children, look inside them.
     * If this tree contains no mapping for the value, returns null.
     *
     * @param value the value whose associated node is to be returned
     * @param node  current node
     * @return the entry inside which the specified value is mapped,
     * or null if this map contains no mapping for the value
     */
    private Entry<K, V> getByValueFrom(Object value, Entry<K, V> node) {
        if ((value == null && node.value == null)
                || (node.getValue().equals(value))) {
            return node;
        } else {
            Entry<K, V> found = null;
            if (node.leftChild != null) {
                found = getByValueFrom(value, node.leftChild);
            }
            if (value != null && node.rightChild != null) {
                found = getByValueFrom(value, node.rightChild);
            }
            return found;
        }
    }

    @Override
    public V get(Object key) {
        if (key == null) {
            return null;
        }
        return getByKeyFrom(key, root);
    }

    /**
     * If finds equal key, returns the value to which the specified key is mapped,
     * otherwise if current node has children, look inside them.
     * If this tree contains no mapping for the key, returns null.
     *
     * @param key  the key whose associated value is to be returned
     * @param node current node
     * @return the value to which the specified key is mapped,
     * or null if this map contains no mapping for the key
     */
    private V getByKeyFrom(Object key, Entry<K, V> node) {
        if (node == null) {
            return null;
        } else if (node.getKey().equals(key)) {
            return node.getValue();
        } else {
            V value = null;
            @SuppressWarnings("unchecked")
            int compare = ((K) key).compareTo(node.key);
            if (compare < 0) {
                if (node.leftChild != null) {
                    value = getByKeyFrom(key, node.leftChild);
                }
            } else if (compare > 0) {
                if (node.rightChild != null) {
                    value = getByKeyFrom(key, node.rightChild);
                }
            }
            return value;
        }
    }

    @Override
    public V put(K key, V value) {
        Entry<K, V> newNode = new Entry<>(key, value);
        if (root == null) {
            root = newNode;
            size = 1;
        } else {
            Entry<K, V> current = root;
            while (true) {
                int compareTo = key.compareTo(current.getKey());
                if (compareTo < 0) {
                    if (current.leftChild == null) {
                        current.leftChild = newNode;
                        size++;
                        break;
                    } else {
                        current = current.leftChild;
                    }
                } else if (compareTo > 0) {
                    if (current.rightChild == null) {
                        current.rightChild = newNode;
                        size++;
                        break;
                    } else {
                        current = current.rightChild;
                    }
                } else {
                    current.setValue(value);
                    break;
                }
            }
        }
        return value;
    }

    @Override
    public V remove(Object key) {
        if (key == null) {
            return null;
        }
        Entry<K, V> current = root;
        Entry<K, V> parent = null;
        boolean isLeftChild = false;

        while (!current.getKey().equals(key)) {
            parent = current;
            @SuppressWarnings("unchecked")
            int compare = ((K) key).compareTo(current.getKey());
            if (compare < 0) {
                if (current.leftChild != null) {
                    current = current.leftChild;
                    isLeftChild = true;
                } else {
                    return null;
                }
            } else if (compare > 0) {
                if (current.rightChild != null) {
                    current = current.rightChild;
                    isLeftChild = false;
                } else {
                    return null;
                }
            }
        }
        return removeFor(current, parent, isLeftChild);
    }

    /**
     * Removes the node depending on the number of its children.
     *
     * @param node        removable {@link Entry} node
     * @param parent      parent of removable {@link Entry} node
     * @param isLeftChild {@code true} if removable node is leftChild child of parent node,
     *                    {@code false} if rightChild or has no parent
     * @return value of removable node
     */
    private V removeFor(Entry<K, V> node, Entry<K, V> parent, boolean isLeftChild) {
        if (node.leftChild == null
                && node.rightChild == null) {
            return removeNodeWithoutChildren(node, parent, isLeftChild);
        } else if (node.leftChild != null && node.rightChild != null) {
            return removeNodeWithTwoChildren(node, parent, isLeftChild);
        } else {
            return removeNodeWithOneChild(node, parent, isLeftChild);
        }
    }

    /**
     * Removes node that contains no child.
     *
     * @param node        removable {@link Entry} node
     * @param parent      parent of removable {@link Entry} node
     * @param isLeftChild {@code true} if removable node is leftChild child of parent node,
     *                    {@code false} if rightChild or has no parent
     * @return value of removable node
     */
    private V removeNodeWithoutChildren(Entry<K, V> node, Entry<K, V> parent, boolean isLeftChild) {
        if (parent == null) {
            root = null;
            return node.getValue();
        }
        if (isLeftChild) {
            parent.leftChild = null;
        } else {
            parent.rightChild = null;
        }
        return node.getValue();
    }

    /**
     * Removes node that contains one child.
     *
     * @param node        removable {@link Entry} node
     * @param parent      parent of removable {@link Entry} node
     * @param isLeftChild {@code true} if removable node is leftChild child of parent node,
     *                    {@code false} if rightChild or has no parent
     * @return value of removable node
     */
    private V removeNodeWithOneChild(Entry<K, V> node, Entry<K, V> parent, boolean isLeftChild) {
        if (node.rightChild == null) {
            if (parent == null) {
                root = node.leftChild;
            } else if (isLeftChild) {
                parent.leftChild = node.leftChild;
            } else {
                parent.rightChild = node.leftChild;
            }
        } else if (node.leftChild == null) {
            if (parent == null) {
                root = node.rightChild;
            } else if (isLeftChild) {
                parent.leftChild = node.rightChild;
            } else {
                parent.rightChild = node.rightChild;
            }
        }
        return node.getValue();
    }

    /**
     * Removes node that contains two children.
     *
     * @param node        removable {@link Entry} node
     * @param parent      parent of removable {@link Entry} node
     * @param isLeftChild {@code true} if removable node is leftChild child of parent node,
     *                    {@code false} if rightChild or has no parent
     * @return value of removable node
     */
    private V removeNodeWithTwoChildren(Entry<K, V> node, Entry<K, V> parent, boolean isLeftChild) {
        Entry<K, V> successor = getSuccessor(node);

        if (node == root) {
            successor.leftChild = root.leftChild;
            if (successor != root.rightChild) {
                successor.rightChild = root.rightChild;
            }
            root = successor;
        } else {
            successor.leftChild = node.leftChild;
            if (isLeftChild) {
                parent.leftChild = successor;
            } else {
                parent.rightChild = successor;
            }
        }
        return node.getValue();
    }

    /**
     * Get successor for removable node.
     *
     * @param delNode {@link Entry} node that will be removed
     * @return successor for removable node
     */
    private Entry<K, V> getSuccessor(Entry<K, V> delNode) {
        Entry<K, V> successor = delNode.rightChild;
        Entry<K, V> parent = delNode;
        while (successor.leftChild != null) {
            parent = successor;
            successor = successor.leftChild;
        }

        if (successor != delNode.rightChild) {
            parent.leftChild = successor.rightChild;
            successor.rightChild = delNode.rightChild;
        }
        return successor;
    }

    @Override
    public void putAll(Map<? extends K, ? extends V> m) {
        for (Map.Entry<? extends K, ? extends V> entry : m.entrySet()) {
            put(entry.getKey(), entry.getValue());
        }
    }

    @Override
    public void clear() {
        root = null;
        size = 0;
    }

    @Override
    public Set<K> keySet() {
        Set<K> keySet = new HashSet<>();
        fillKeySetFrom(keySet, root);
        return keySet;
    }

    /**
     * Fills keySet recursively.
     *
     * @param keySet set of keys to be filled.
     * @param node   current {@link Entry} node
     */
    private void fillKeySetFrom(Collection<K> keySet, Entry<K, V> node) {
        keySet.add(node.key);
        if (node.leftChild != null) {
            fillKeySetFrom(keySet, node.leftChild);
        }
        if (node.rightChild != null) {
            fillKeySetFrom(keySet, node.rightChild);
        }
    }

    @Override
    public Collection<V> values() {
        Collection<V> values = new HashSet<>();
        fillValuesSetFrom(values, root);
        return values;
    }

    /**
     * Fills values Set recursively.
     *
     * @param values set of values to be filled.
     * @param node   current {@link Entry} node
     */
    private void fillValuesSetFrom(Collection<V> values, Entry<K, V> node) {
        values.add(node.value);
        if (node.leftChild != null) {
            fillValuesSetFrom(values, node.leftChild);
        }
        if (node.rightChild != null) {
            fillValuesSetFrom(values, node.rightChild);
        }
    }

    @Override
    public Set<Map.Entry<K, V>> entrySet() {
        Set<Map.Entry<K, V>> entrySet = new HashSet<>();
        fillEntrySetFrom(entrySet, root);
        return entrySet;
    }

    /**
     * Fills entrySet recursively.
     *
     * @param entrySet set of entries to be filled.
     * @param node     current {@link Entry} node
     */
    private void fillEntrySetFrom(Collection<Map.Entry<K, V>> entrySet, Entry<K, V> node) {
        entrySet.add(node);
        if (node.leftChild != null) {
            fillEntrySetFrom(entrySet, node.leftChild);
        }
        if (node.rightChild != null) {
            fillEntrySetFrom(entrySet, node.rightChild);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BinaryTree<?, ?> that = (BinaryTree<?, ?>) o;
        return size == that.size &&
                Objects.equals(root, that.root);
    }

    @Override
    public int hashCode() {
        return Objects.hash(root, size);
    }

    /**
     * Prints binary three using {@link TreePrinter}.
     */
    public void print() {
        TreePrinter.print(root);
    }
}
