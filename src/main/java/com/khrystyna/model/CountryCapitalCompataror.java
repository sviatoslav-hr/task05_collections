package com.khrystyna.model;

import java.util.Comparator;
import java.util.function.Function;
import java.util.function.ToDoubleFunction;
import java.util.function.ToIntFunction;
import java.util.function.ToLongFunction;

public class CountryCapitalCompataror implements Comparator<Country> {
    @Override
    public int compare(Country o1, Country o2) {
        return o1.getCapitalName().compareTo(o2.getCapitalName());
    }

    @Override
    public Comparator<Country> reversed() {
        return null;
    }

    @Override
    public Comparator<Country> thenComparing(Comparator<? super Country> other) {
        return null;
    }

    @Override
    public <U> Comparator<Country> thenComparing(Function<? super Country, ? extends U> keyExtractor, Comparator<? super U> keyComparator) {
        return null;
    }

    @Override
    public <U extends Comparable<? super U>> Comparator<Country> thenComparing(Function<? super Country, ? extends U> keyExtractor) {
        return null;
    }

    @Override
    public Comparator<Country> thenComparingInt(ToIntFunction<? super Country> keyExtractor) {
        return null;
    }

    @Override
    public Comparator<Country> thenComparingLong(ToLongFunction<? super Country> keyExtractor) {
        return null;
    }

    @Override
    public Comparator<Country> thenComparingDouble(ToDoubleFunction<? super Country> keyExtractor) {
        return null;
    }
}
