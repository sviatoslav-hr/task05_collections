package com.khrystyna.model;

public class StringContainer {
    private static final int INITIAL_SIZE = 3;
    private static final double SIZE_MULTIPLIER= 1.5;
    private int size;
    private String[] array;

    public StringContainer() {
        array = new String[INITIAL_SIZE];
    }

    public void add(String s) {
        int position = size;
        if (size + 1 > array.length) {
            expandArray();
        }
        array[position] = s;
        size++;
    }

    public String get(int index) {
        if (index >= 0 && index < size) {
            return array[index];
        } else {
            throw new ArrayIndexOutOfBoundsException();
        }
    }

    private void expandArray() {
        int newLength = (int) ((double)array.length * SIZE_MULTIPLIER + 1);
        String[] newArray = new String[newLength];
        System.arraycopy(array, 0, newArray, 0, array.length);
        array = newArray;
    }

}
