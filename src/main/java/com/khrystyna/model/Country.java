package com.khrystyna.model;

import java.util.Objects;

public class Country implements Comparable<Country> {
    private String name;
    private String capitalName;

    public Country(String countryName, String capitalName) {
        this.name = countryName;
        this.capitalName = capitalName;
    }

    public String getName() {
        return name;
    }

    String getCapitalName() {
        return capitalName;
    }

    @Override
    public int compareTo(Country o) {
        return name.compareTo(o.name);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Country country = (Country) o;
        return Objects.equals(name, country.name) &&
                Objects.equals(capitalName, country.capitalName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, capitalName);
    }

    @Override
    public String toString() {
        return "Country{" + name +
                ", capital: " + capitalName +
                '}';
    }
}
