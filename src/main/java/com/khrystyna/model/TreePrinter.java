package com.khrystyna.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

class TreePrinter {
    /**
     * Node that can be printed.
     */
    public interface PrintableNode {
        /**
         * Gets left child.
         */
        PrintableNode getLeftChild();

        /**
         * Gets right child.
         */
        PrintableNode getRightChild();

        /**
         * Gets text to be printed.
         */
        String getText();
    }

    /**
     * Prints a tree.
     *
     * @param root tree root node
     */
    static void print(PrintableNode root) {
        List<List<String>> lines = new ArrayList<>();

        int perPiece = fillLinesAndGetPerPiece(lines, root);

        for (int i = 0; i < lines.size(); i++) {
            List<String> line = lines.get(i);
            int hpw = (int) Math.floor(perPiece / 2f) - 1;

            if (i > 0) {
                printBranchesLine(line, perPiece, hpw);
            }

            printNumbersLine(line, perPiece);

            perPiece /= 2;
        }
    }

    private static int fillLinesAndGetPerPiece(List<List<String>> lines, PrintableNode root) {
        List<PrintableNode> level = new ArrayList<>(Collections.singletonList(root));
        List<PrintableNode> next = new ArrayList<>();

        int childrenNumber = 1;
        int widestLength = 0;

        while (childrenNumber != 0) {
            List<String> line = new ArrayList<>();
            childrenNumber = 0;
            for (PrintableNode levelPart : level) {
                if (levelPart == null) {
                    line.add(null);
                    next.add(null);
                    next.add(null);
                } else {
                    String text = levelPart.getText();
                    line.add(text);
                    if (text.length() > widestLength) {
                        widestLength = text.length();
                    }
                    next.add(levelPart.getLeftChild());
                    next.add(levelPart.getRightChild());
                    if (levelPart.getLeftChild() != null) childrenNumber++;
                    if (levelPart.getRightChild() != null) childrenNumber++;
                }
            }
            if (widestLength % 2 == 1) {
                widestLength++;
            }
            lines.add(line);
            List<PrintableNode> tmp = level;
            level = next;
            next = tmp;
            next.clear();
        }
        return lines.get(lines.size() - 1).size() * (widestLength + 4);
    }

    private static void printBranchesLine(List<String> line, int perPiece, int hpw) {
        for (int j = 0; j < line.size(); j++) {
            // split node
            char c = ' ';
            if (j % 2 == 1) {
                if (line.get(j - 1) != null) {
                    c = (line.get(j) != null) ? '┴' : '┘';
                } else {
                    if (line.get(j) != null) c = '└';
                }
            }
            System.out.print(c);

            // lines and spaces
            if (line.get(j) == null) {
                for (int k = 0; k < perPiece - 1; k++) {
                    System.out.print(" ");
                }
            } else {
                for (int k = 0; k < hpw; k++) {
                    System.out.print(j % 2 == 0 ? " " : "─");
                }
                System.out.print(j % 2 == 0 ? "┌" : "┐");
                for (int k = 0; k < hpw; k++) {
                    System.out.print(j % 2 == 0 ? "─" : " ");
                }
            }

        }
        System.out.println();
    }

    private static void printNumbersLine(List<String> line, int perPiece) {
        // print line of numbers
        for (String f : line) {
            if (f == null) {
                f = "";
            }
            float res = perPiece / 2f - f.length() / 2f;
            int gap1 = (int) Math.ceil(res);
            int gap2 = (int) Math.floor(res);

            // a number
            for (int k = 0; k < gap1; k++) {
                System.out.print(" ");
            }
            System.out.print(f);
            for (int k = 0; k < gap2; k++) {
                System.out.print(" ");
            }
        }
        System.out.println();
    }
}
