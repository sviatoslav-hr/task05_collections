package com.khrystyna.model;

import java.util.*;
import java.util.function.Consumer;

public class CustomDeque<E> implements Deque<E> {
    private Node<E> first;
    private Node<E> last;
    private int size;

    private class Node<T> {
        final T item;
        Node<T> prev;
        Node<T> next;

        private Node(Node<T> previous, T element, Node<T> next) {
            this.item = element;
            this.prev = previous;
            this.next = next;
        }
    }

    @Override
    public void addFirst(E e) {
        Node<E> exFirst = first;
        first = new Node<>(null, e, exFirst);
        if (exFirst == null) {
            last = first;
        } else {
            exFirst.prev = first;
        }
        size++;
    }

    @Override
    public void addLast(E e) {
        Node<E> exLast = last;
        first = new Node<>(exLast, e, null);
        size++;
    }

    @Override
    public boolean offerFirst(E e) {
        addFirst(e);
        return true;
    }

    @Override
    public boolean offerLast(E e) {
        addLast(e);
        return true;
    }

    @Override
    public E removeFirst() {
        if (size == 0) {
            return null;
        }
        return unlink(first);
    }

    @Override
    public E removeLast() {
        if (size == 0) {
            return null;
        }
        return unlink(last);
    }

    @Override
    public E pollFirst() {
        return removeFirst();
    }

    @Override
    public E pollLast() {
        return removeLast();
    }

    @Override
    public E getFirst() {
        return first.item;
    }

    @Override
    public E getLast() {
        return last.item;
    }

    @Override
    public E peekFirst() {
        return getFirst();
    }

    @Override
    public E peekLast() {
        return getLast();
    }

    @Override
    public boolean removeFirstOccurrence(Object o) {
        if (o == null) {
            for (Node<E> node = first; node != null; node = node.next) {
                if (node.item == null) {
                    unlink(node);
                    return true;
                }
            }
        } else {
            for (Node<E> node = first; node != null; node = node.next) {
                if (o.equals(node.item)) {
                    unlink(node);
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public boolean removeLastOccurrence(Object o) {
        if (o == null) {
            for (Node<E> node = last; node != null; node = node.prev) {
                if (node.item == null) {
                    unlink(node);
                    return true;
                }
            }
        } else {
            for (Node<E> node = last; node != null; node = node.prev) {
                if (node.item.equals(o)) {
                    unlink(node);
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public boolean add(E e) {
        addFirst(e);
        return true;
    }

    @Override
    public boolean offer(E e) {
        return offerFirst(e);
    }

    @Override
    public E remove() {
        return removeFirst();
    }

    @Override
    public E poll() {
        return pollFirst();
    }

    @Override
    public E element() {
        return getFirst();
    }

    @Override
    public E peek() {
        return peekFirst();
    }

    @Override
    public void push(E e) {
        addFirst(e);
    }

    @Override
    public E pop() {
        return remove();
    }

    @Override
    public boolean remove(Object o) {
        return removeFirstOccurrence(o);
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        for (Object o : c) {
            if (!contains(o)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        boolean changed = false;
        for (E e : c) {
            if (add(e)) {
                changed = true;
            }
        }
        return changed;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        boolean changed = false;
        for (Object e : c) {
            if (remove(e)) {
                changed = true;
            }
        }
        return changed;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        boolean changed = false;
        for (Node<E> node = first; node != null; node = node.next) {
            if (!c.contains(node.item)) {
                unlink(node);
                changed = true;
            }
        }
        return changed;
    }

    @Override
    public void clear() {
        first = null;
        last = null;
        size = 0;
    }

    @Override
    public boolean contains(Object o) {
        if (o == null) {
            for (Node<E> node = first; node != null; node = node.next) {
                if (node.item == null) {
                    return true;
                }
            }
        } else {
            for (Node<E> node = first; node != null; node = node.next) {
                if (o.equals(node.item)) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public Iterator<E> iterator() {
        return new Iterator<E>() {
            private int index;
            private Node<E> node;

            @Override
            public boolean hasNext() {
                return index < size;
            }

            @Override
            public E next() {
                if (node == null) {
                    node = first;
                } else {
                    node = node.next;
                }
                index++;
                return node.item;
            }

            @Override
            public void remove() {
                unlink(node);
            }
        };
    }

    @Override
    public Object[] toArray() {
        Object[] objects = new Object[size];
        int i = 0;
        for (Node<E> node = first; node != null; node = node.next) {
            objects[i++] = node.item;
        }
        return objects;
    }

    @Override
    public <T> T[] toArray(T[] a) {
        if (a.length < size) {
            throw new ArrayIndexOutOfBoundsException("Array's size is too small" +
                    " to store elements of the deque");
        }
        int i = 0;
        for (Node<E> node = first; node != null; node = node.next) {
            ((Object[]) a)[i++] = node.item;
        }
        if (a.length > size) {
            while (i <= a.length) {
                ((Object[]) a)[i++] = null;
            }
        }
        return a;
    }

    @Override
    public Iterator<E> descendingIterator() {
        return new Iterator<E>() {
            private int index;
            private Node<E> node = last;

            @Override
            public boolean hasNext() {
                return index < size;
            }

            @Override
            public E next() {
                if (node == null) {
                    node = first;
                } else {
                    node = node.prev;
                }
                index++;
                return node.item;
            }

            @Override
            public void remove() {
                unlink(node);
            }
        };
    }

    private E unlink(Node<E> node) {
        E elem = node.item;
        Node<E> prevNode = node.prev;
        Node<E> nextNode = node.next;

        if (prevNode == null) {
            first = nextNode;
        } else {
            prevNode.next = nextNode;
            node.prev = null;
        }

        if (nextNode == null) {
            last = prevNode;
        } else {
            nextNode.prev = prevNode;
            node.next = null;
        }
        size--;
        return elem;
    }
}
