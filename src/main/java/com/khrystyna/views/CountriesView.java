package com.khrystyna.views;

import com.khrystyna.controllers.CountriesController;
import com.khrystyna.model.Country;
import com.khrystyna.model.CountryCapitalCompataror;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.stream.Collectors;

public class CountriesView implements View {
    private CountriesController controller = new CountriesController();
    private static Logger logger = LogManager.getLogger(CountriesView.class);

    @Override
    public void run() {
        List<Country> countryList = controller.getRandomCountriesList(20);
        logger.debug(countryList.size());
        for (Country country : countryList) {
            logger.trace(country);
        }
        printSortedByName(countryList);
        printSortedByCapitalName(countryList);
    }

    private void printSortedByName(List<Country> countries) {
        countries = countries.stream()
                .sorted(Country::compareTo)
                .collect(Collectors.toList());
        logger.trace("\nSorted by name:");
        for (Country country : countries) {
            logger.trace(country);
        }
    }

    private void printSortedByCapitalName(List<Country> countries) {
        CountryCapitalCompataror comparator = new CountryCapitalCompataror();
        countries = countries.stream()
                .sorted(comparator)
                .collect(Collectors.toList());
        logger.trace("\nSorted by capital name:");
        for (Country country : countries) {
            logger.trace(country);
        }
    }
}
