package com.khrystyna.views;

import com.khrystyna.controllers.CountriesController;
import com.khrystyna.model.Country;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Deque;

public class DequeView implements View {
    private static Logger logger = LogManager.getLogger(DequeView.class);
    private CountriesController controller = new CountriesController();
    @Override
    public void run() {
        Deque<Country> deque = controller.getRandomCountriesDeque(20);

        for (Country next : deque) {
            logger.trace(next);
        }

        logger.trace(deque.removeFirst());
        logger.trace(deque.size());

        logger.trace(deque.removeLast());
        logger.trace(deque.size());

        logger.trace(deque.removeFirstOccurrence(
                new Country("CHINA", "BEIJING")));
        logger.trace(deque.size());

        logger.trace(deque.removeLastOccurrence(
                new Country("CHINA", "BEIJING")));
        logger.trace(deque.size());
    }
}
