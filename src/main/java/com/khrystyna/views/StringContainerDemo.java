package com.khrystyna.views;

import com.khrystyna.model.StringContainer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Scanner;

public class StringContainerDemo {
    private static Logger logger = LogManager.getLogger(StringContainerDemo.class);

    public static void main(String[] args) {

        System.out.print("Type elements size: ");
        int elementsSize = new Scanner(System.in).nextInt();
        testArrayList(elementsSize);
        testStringContainer(elementsSize);
    }

    private static void testArrayList(int size) {
        long time = System.currentTimeMillis();
        ArrayList<String> list = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            list.add("Test");
        }
        time = System.currentTimeMillis() - time;
        logger.trace("ArrayList insertion time: " + time + "ms");
    }

    private static void testStringContainer(int size) {
        long time = System.currentTimeMillis();
        StringContainer list = new StringContainer();
        for (int i = 0; i < size; i++) {
            list.add("Test");
        }
        time = System.currentTimeMillis() - time;
        logger.trace("StringContainer insertion time: " + time + "ms");
    }
}
