package com.khrystyna.views.menu.mapped;

import com.khrystyna.controllers.BinaryTreeController;
import com.khrystyna.views.View;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MapMenu implements View {
    private static Logger logger = LogManager.getLogger(MapMenu.class);
    private Scanner scanner = new Scanner(System.in);
    private Map<Integer, MenuItem> menu = new LinkedHashMap<>();
    private BinaryTreeController controller = new BinaryTreeController();
    private boolean exit;

    @Override
    public void run() {
        prepareMenu();

        do {
            displayMenu();
            int index = getInput("Enter number: ");
            MenuItem menuItem = menu.get(index);
            if (menuItem != null) {
                menuItem.getAction().proceed();
            }
        } while (!exit);

    }

    private void prepareMenu() {
        menu.put(1, new MenuItem("Generate binary tree", this::generateTree));
        menu.put(2, new MenuItem("Print tree", controller::printTree));
        menu.put(3, new MenuItem("Insert element", this::insertElement));
        menu.put(4, new MenuItem("Search element", this::findElement));
        menu.put(5, new MenuItem("Remove element", this::removeElement));
        menu.put(6, new MenuItem("Clear tree", controller::clear));
        menu.put(0, new MenuItem("exit", () -> exit = true));
    }

    private void displayMenu() {
        for (Map.Entry<Integer, MenuItem> entry : menu.entrySet()) {
            logger.trace(entry.getKey() + "\t-\t"
                    + entry.getValue().getDescription());
        }
    }

    private void generateTree() {
        int size = getInput("Type number of elements to insert: ");
        controller.generateTree(size);
    }

    private void insertElement() {
        int key = getInput("Type key: ");
        int value = getInput("Type value: ");
        controller.insert(key, value);
    }

    private void findElement() {
        int key = getInput("Type key: ");
        Object elem = controller.find(key);
        if (elem != null) {
            logger.trace(key + ":" + elem + " was found");
        } else {
            logger.trace("Element with key " + key + " was not found!");
        }
    }

    private void removeElement() {
        int key = getInput("Type key: ");
        Object elem = controller.remove(key);
        if (elem != null) {
            logger.trace(key + ":" + elem + " was removed");
        } else {
            logger.trace("Element with key " + key + " was not found!");
        }
    }

    private int getInput(String message) {
        System.out.print(message);
        return scanner.nextInt();
    }
}
