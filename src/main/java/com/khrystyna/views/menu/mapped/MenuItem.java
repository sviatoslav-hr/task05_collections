package com.khrystyna.views.menu.mapped;

import com.khrystyna.views.menu.Printable;

public class MenuItem {
    private String description;
    private Printable action;

    MenuItem(String description, Printable action) {
        this.description = description;
        this.action = action;
    }

    String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    Printable getAction() {
        return action;
    }

    public void setAction(Printable action) {
        this.action = action;
    }
}
