package com.khrystyna.views.menu.enumerated;

import com.khrystyna.views.View;
import com.khrystyna.views.menu.mapped.MapMenu;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class EnumMenu implements View {
    private static Logger logger = LogManager.getLogger(MapMenu.class);
    private Scanner scanner = new Scanner(System.in);
    private List<MenuItem> menu = new ArrayList<>();
    private boolean exit;

    @Override
    public void run() {
        prepareMenu();

        do {
            displayMenu();
            int index = getInput();
            MenuItem menuItem = menu.get(index);
            if (menuItem != null) {
                menuItem.getAction().proceed();
            }
        } while (!exit);

    }

    private void prepareMenu() {
//        menu.put(1, new MenuItem("some action...", this::act));
//        menu.put(2, new MenuItem("other action action...", this::act));
//        menu.put(0, new MenuItem("exit", () -> exit = true));
    }

    private void displayMenu() {
//        for (Map.Entry<Integer, MenuItem> entry : menu.entrySet()) {
//            logger.trace(entry.getKey() + "\t-\t"
//                    + entry.getValue().getDescription());
//        }
    }

    private int getInput() {
        System.out.print("Enter number: ");
        return scanner.nextInt();
    }

    private void act() {
        logger.trace("hey");
    }

}
