package com.khrystyna.controllers;

import com.khrystyna.model.BinaryTree;

import java.util.Random;

public class BinaryTreeController {
    private BinaryTree<Integer, Integer> tree;

    public void generateTree(int size) {
        tree = new BinaryTree<>();
        Random random = new Random();
        final int keyBound = 100;
        final int valueBound = 10000;
        for (int i = 0; i < size; i++) {
            tree.put(random.nextInt(keyBound), random.nextInt(valueBound));
        }
    }

    public void printTree() {
        if (tree != null) {
            tree.print();
        }
    }

    public void insert(int key, int value) {
        tree.put(key, value);
    }

    public void clear() {
        tree.clear();
    }

    public Object remove(int key) {
        return tree.remove(key);
    }

    public Object find(int key) {
        return tree.get(key);
    }
}
