package com.khrystyna.controllers;

import com.khrystyna.model.Country;
import com.khrystyna.model.CountryGenerator;
import com.khrystyna.model.CustomDeque;

import java.util.ArrayList;
import java.util.Deque;
import java.util.List;

public class CountriesController {

    public List<Country> getRandomCountriesList(int size) {
        CountryGenerator generator = new CountryGenerator();

        List<Country> countries = new ArrayList<>();

        for (int i = 0; i < size; i++) {
            countries.add(generator.getNextCountry());
        }
        return countries;
    }

    public Deque<Country> getRandomCountriesDeque(int size) {
        CountryGenerator generator = new CountryGenerator();

        Deque<Country> countries = new CustomDeque<>();

        for (int i = 0; i < size; i++) {
            countries.add(generator.getNextCountry());
        }
        return countries;
    }
}
