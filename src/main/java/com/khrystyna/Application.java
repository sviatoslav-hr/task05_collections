package com.khrystyna;

import com.khrystyna.views.*;
import com.khrystyna.views.menu.mapped.MapMenu;

public class Application {
    public static void main(String[] args) {
//        View view = new ConsoleView();
//        View view = new CountriesView();
//        View view = new DequeView();
        View view = new MapMenu();

        view.run();
    }
}